package com.example.task10;

public class Task10 {
	public static boolean compare(float a, float b, int precision) {
		// TODO корректно сравнивать два значения типа float с заданной пользователем
		// точностью (параметр - количество знаков после запятой).
		// Функция должна корректно обрабатывать ситуацию со сравнением значений
		// бесконечности.
		// Функция должна считать значения «не число» NaN (например 0.0/0.0) равными
		// между собой.

		if (b == Float.POSITIVE_INFINITY || b == Float.NEGATIVE_INFINITY || Float.isNaN(b)) {
			float tmp = a;
			a = b;
			b = tmp;
		}

		if (a == Float.POSITIVE_INFINITY) {
			return b == Float.POSITIVE_INFINITY;
		}

		if (a == Float.NEGATIVE_INFINITY) {
			return b == Float.NEGATIVE_INFINITY;
		}

		if (Float.isNaN(a)) {
			return Float.isNaN(b);
		}

		int bits = Float.floatToIntBits(a);
		int actual_precision = (bits >> 23) & ((bits << 8) - 1) - 127;
		int min_precision = Math.min(precision, actual_precision);

		float exp = (float) Math.pow(10, (double) min_precision);
		float fa = (float) Math.floor(a * exp) / exp;
		float fb = (float) Math.floor(b * exp) / exp;

		return fa == fb;
	}

	public static void main(String[] args) {
		float a = 100.001f;
		float b = 100.009f;

		boolean result = compare(a, b, 2);
		System.out.println(result);
	}
}
