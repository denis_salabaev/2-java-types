package com.example.task14;

public class Task14 {
	public static int reverse(int value) {
		int result = 0;

		do {
			int rem = value % 10;
			result = result * 10 + rem;
			value /= 10;
		} while (value != 0);

		return result;
	}

	public static void main(String[] args) {
		// Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый
		// метод и смотря результат
		// например вот так:
		int result = reverse(345);
		System.out.println(result);
	}
}
